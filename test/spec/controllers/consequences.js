'use strict';

describe('Controller: ConsequencesCtrl', function () {

    // load the controller's module
    beforeEach(module('flagisv3App'));

    var ConsequencesCtrl,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        ConsequencesCtrl = $controller('ConsequencesCtrl', {
            $scope: scope
        });
    }));

    it('should attach a list of awesomeThings to the scope', function () {
        expect(scope.awesomeThings.length).toBe(3);
    });
});
