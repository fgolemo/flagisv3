'use strict';

angular.module('flagisv3App', [
        'ngRoute',
        'ngDebounce',
        'angularMoment',
        'ui.select2',
        'flagisv3App.directives',
        'flagisv3App.factories',
        'flagisv3App.filters',
        'flagisv3App.controllers'
        //'activeDomain', //loaded later
        //'array', //loaded later
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/notes', {
                templateUrl: 'views/notes.html',
                controller: 'NotesCtrl'
            })
            .when('/sources', {
                templateUrl: 'views/sources.html',
                controller: 'SourcesCtrl'
            })
            .when('/consequences', {
                templateUrl: 'views/consequences.html',
                controller: 'ConsequencesCtrl'
            })
            .otherwise({
                redirectTo: '/notes'
            });
    }).run(['uiSelect2Config', function (uiSelect2Config) {
        uiSelect2Config.tokenSeparators = [",", " "];
        uiSelect2Config.multiple = true;
        uiSelect2Config.simple_tags = true;
    }]);