'use strict';

describe('Filter: activeDomain', function () {

    // load the filter's module
    beforeEach(module('flagisv3App'));

    // initialize a new instance of the filter before each test
    var activeDomain;
    beforeEach(inject(function ($filter) {
        activeDomain = $filter('activeDomain');
    }));

    it('should return the input prefixed with "activeDomain filter:"', function () {
        var text = 'angularjs';
        expect(activeDomain(text)).toBe('activeDomain filter: ' + text);
    });

});
