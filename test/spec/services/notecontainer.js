'use strict';

describe('Service: Notecontainer', function () {

    // load the service's module
    beforeEach(module('flagisv3App'));

    // instantiate service
    var Notecontainer;
    beforeEach(inject(function (_Notecontainer_) {
        Notecontainer = _Notecontainer_;
    }));

    it('should do something', function () {
        expect(!!Notecontainer).toBe(true);
    });

});
