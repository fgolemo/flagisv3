'use strict';

describe('Filter: general', function () {

    // load the filter's module
    beforeEach(module('flagisv3App'));

    // initialize a new instance of the filter before each test
    var general;
    beforeEach(inject(function ($filter) {
        general = $filter('general');
    }));

    it('should return the input prefixed with "general filter:"', function () {
        var text = 'angularjs';
        expect(general(text)).toBe('general filter: ' + text);
    });

});
